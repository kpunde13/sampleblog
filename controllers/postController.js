const Post= require('./../models/posts')

module.exports.createpostGET=function(req, res){
    console.log(req.url);
    return res.render('./Post/createPost');
}

module.exports.createpostPOST= (req, res)=>{
    console.log(req.url)
    post_title= req.body.post_title;
    post_content= req.body.post_content;

    Post.findOne({title: post_title}, (err, post_received)=>{
        if(err){console.log.bind(console, 'Error in controllers.postController.createpostPOST.findOne: ', err)}
        else{
            if(post_received){
                req.flash('alert', 'Duplicate post found with same Title.')
                return res.render('./Post/createPost')
            }else{
                Post.create({
                    title: post_title,
                    content: post_content,
                    user: req.user._id
                }, (err, post)=>{
                    if(err){console.error.bind(console, 'Error received in controlers.postController.createpostPOST.create: ', err); return;}
                    else{
                        console.log('Post created successfully: ', post);
                        req.flash('alert', 'Post created successfully !')
                        return res.redirect('/');
                    }
                })
            }
        }
    })
}
