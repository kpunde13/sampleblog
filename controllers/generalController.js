const Post= require('./../models/posts')

module.exports.homeGET= (req, res)=>{
    console.log(req.url);
    Post.find({}).populate('user').exec((err, posts)=>{
        if(err){console.error.bind(console, 'Error received in controllers.generalControler.homeGET.find(): ', err); return;}
        else{
            res.render('./generalPages/home', {
                title: 'Home',
                posts: posts,
                flashmsg: req.flash('alert'),
            });
        }
    })
}

