const User= require('./../models/users')
const bcrypt= require('bcrypt')
const bcryptRounds= 10;
const flash= require('connect-flash');

module.exports.signinGET= (req, res)=>{
    console.log(req.url)
    res.render('./authPages/signin', {
        title: 'Sign-in',
        flashmsg: req.flash('alert'),
    })
}

module.exports.signinPOST= (req, res)=>{
    console.log(req.url);
    return res.redirect('/');
}

module.exports.signupGET= (req, res)=>{
    console.log(req.url)
    res.render('./authPages/signup', {
        title: 'Sign-up',
        flashmsg: req.flash('alert'),
    })
}

module.exports.signupPOST= (req, res)=>{
    console.log(req.url)
    if(req.body.password != req.body.confirm_password){
        req.flash('alert', 'Password and confirm password do not match !')
        return res.redirect('back');
    }else{
        User.findOne({email: req.body.email}, (err, user)=>{
            if(err){console.error.bind(console, 'Error received in controller.authController.SignupPOST.findone: ', err); return;}
            else{
                if(user){
                    req.flash('alert', 'Email already in use !')
                    return res.redirect('back');
                }else{
                    User.findOne({username: req.body.username}, (err, user_filtered)=>{
                        if(err){console.error.bind(console, 'Error received in controller.authController.SignupPOST.findone: ', err); return;}
                        else{
                            if(user_filtered){
                                req.flash('alert', 'Username already in use !')
                                return res.redirect('back');
                            }else{
                                bcrypt.hash(req.body.password, bcryptRounds, (err, hash)=>{
                                    if(err){console.error.bind(console, 'Error received in controller.authController.SignupPOST.Bcrypt: ', err); return;}
                                    else{
                                        User.create({
                                            username: req.body.username,
                                            email: req.body.email,
                                            password: hash
                                        }, (err, user_final)=>{
                                            if(err){console.error.bind(console, 'Error received in controller.authController.SignupPOST.User.create: ', err); return;}
                                            else{
                                                console.log('New user created', user_final);
                                                req.flash('alert', 'New user created !')
                                                return res.redirect('/');
                                            }
                                        })
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    }
}

module.exports.signoutGET= (req, res)=>{
    console.log(req.url);
    req.logout();
    req.flash('alert', 'Successfully signed off  !')
    res.redirect('/');
}