const authController=require('./../controllers/authController');
const express= require('express');
const passport= require('passport');
const router= express.Router();

router.get('/signin', authController.signinGET)
router.post('/signin', passport.authenticate('local', {failureRedirect: '/auth/signin', failureFlash: true}), authController.signinPOST)
router.get('/signup', authController.signupGET)
router.post('/signup', authController.signupPOST)
router.get('/signout', authController.signoutGET)

module.exports= router