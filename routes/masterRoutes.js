const generalRoutes= require('./generalRoutes')
const authRoutes= require('./authRoutes')
const postRoutes= require('./postRoutes')
const express= require('express');
const router= express.Router();

router.use('/', generalRoutes);
router.use('/auth', authRoutes);
router.use('/post', postRoutes);

module.exports= router