const postController=require('./../controllers/postController');
const express= require('express');
const passport= require('passport');
const router= express.Router();


//Everything works good with (route -- router.get('/create', postController.createpostGET)) .
//However error is received on using "passport.checkAuthentication" method
//Error thrown by console :-
/* Error: Route.get() requires a callback function but got a [object Undefined]
at Route.(anonymous function) [as get] (E:\GoogleDrive\Projects\NodeJS\SampleBlog\node_modules\express\lib\router\route.js:202:15)
at Function.proto.(anonymous function) [as get] (E:\GoogleDrive\Projects\NodeJS\SampleBlog\node_modules\express\lib\router\index.js:510:19)
at Object.<anonymous> (E:\GoogleDrive\Projects\NodeJS\SampleBlog\routes\postRoutes.js:9:8)
at Module._compile (internal/modules/cjs/loader.js:701:30)
at Object.Module._extensions..js (internal/modules/cjs/loader.js:712:10)
at Module.load (internal/modules/cjs/loader.js:600:32)
at tryModuleLoad (internal/modules/cjs/loader.js:539:12)
at Function.Module._load (internal/modules/cjs/loader.js:531:3)
at Module.require (internal/modules/cjs/loader.js:637:17)
at require (internal/modules/cjs/helpers.js:22:18)
at Object.<anonymous> (E:\GoogleDrive\Projects\NodeJS\SampleBlog\routes\masterRoutes.js:3:19)
at Module._compile (internal/modules/cjs/loader.js:701:30)
at Object.Module._extensions..js (internal/modules/cjs/loader.js:712:10)
at Module.load (internal/modules/cjs/loader.js:600:32)
at tryModuleLoad (internal/modules/cjs/loader.js:539:12)
at Function.Module._load (internal/modules/cjs/loader.js:531:3)
*/

router.get('/create', postController.createpostGET)
router.post('/create', postController.createpostPOST)

module.exports= router