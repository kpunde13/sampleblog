const generalController= require('./../controllers/generalController')
const express= require('express');
const router= express.Router();

router.get('/', generalController.homeGET)

module.exports= router