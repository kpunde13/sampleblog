const db= require('./config/mongoose')
const masterRoutes= require('./routes/masterRoutes')
const express= require('express');
const path= require('path');
const session= require('express-session');
const passport= require('passport');
const passportlocal= require('./config/passport')
const redisStrore= require('connect-redis')(session);
const cookieParser= require('cookie-parser');
const flash= require('connect-flash');

//Server Initializing Parameters
const port= 8000;
const app= express();

app.use(express.urlencoded());

//Setting Server Config
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(cookieParser())
app.use(session({
    name: 'SampleBlog',
    secret: 'SampleBlogRandomKey',
    saveUninitialized: false,
    resave: false,
    cookie: {
        maxAge: (1000*60*100)
    },
    store: new redisStrore({
        host: '172.30.30.171',
        port: 6379,
        ttl :  260,
    })
}));
app.use(flash())

app.use(passport.initialize());
app.use(passport.session());
app.use(passport.setAuthenticatedUser);

//Routes
app.use(masterRoutes);
app.use((req, res, next)=>{
    console.log(req.url);
    res.render('./generalPages/404Error', {
        title: '404 Error',
    });
})

//Starting Server
app.listen(port, function(err){
    if (err){
        console.error.bind(console, 'Error in starting the Server on: ', port);
    }else{
        console.log('Server started successfully on port: ', port);
    }
});