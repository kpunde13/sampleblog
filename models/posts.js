const mongoose= require('mongoose');
const schema= mongoose.Schema;

const postSChema= new schema({
    title: {
        type: 'String',
        required: true,
        unique: true,
    },
    content: {
        type: 'String',
        required: true,
    },
    user: {
        type: schema.ObjectId,
        ref: 'User',
    }
},{
    timestamps: true,
})

const Post= mongoose.model('Post', postSChema);
module.exports= Post;