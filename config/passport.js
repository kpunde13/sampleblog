const User= require('./../models/users');
const bcrypt= require('bcrypt');
const passport=require('passport');
const localStrategy= require('passport-local').Strategy;
 
passport.use(new localStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback : true,   
}, function(req, email, password, done){
    User.findOne({email: email}, function(err, user){
        if(err){console.log('Error received in config.passport.findOne: ', err); return done(err);}
        else{
            if(!user || !(bcrypt.compareSync(password, user.password))){
                console.log('Authentication Failure.');
                return done(null, false,  req.flash('alert', 'Incorrect Credentials.') );
            }else{
                console.log('Authentication Successfull for:  ', user)
                return done(null, user, req.flash('alert', 'Welcome back: ', user.username));
            }
        }
    });
}));

passport.serializeUser(function(user, done){
    done(null, user.id);
});

passport.deserializeUser(function(id, done){
    User.findById(id, (err, user)=>{
        if(err){console.error.bind(console, 'Error received in config.passport.deserializationUser: ', err); return done(err)}
        else{
            return done(null, user);
        }
    });
});

passport.checkAuthentication = function(req, res, next){
    if (req.isAuthenticated()){
        return next();
    }else{
        return res.redirect('/auth/sigin');
    }
}

passport.setAuthenticatedUser = function(req, res, next){
    if (req.isAuthenticated()){
        res.locals.user = req.user;
    }
    next();
}

module.exports= passport